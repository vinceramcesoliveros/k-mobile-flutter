class FakeWeeklyData {
  static const List<String> fakeWeeklyData = [
    "Week One Lunch Menu",
    "Week Two Lunch Menu",
    "Week Three Lunch Menu",
    "Week Four Lunch Menu",
    "Week Five Lunch Menu",
  ];
  static const List<String> fakeDays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"
  ];
  static List<Product> products = [
    Product(item: "Chop Suey", quantity: 1, price: 50, isAvailable: true),
    Product(item: "Chop Suey", quantity: 1, price: 50, isAvailable: true),
    Product(item: "Chop Suey", quantity: 1, price: 50, isAvailable: true),
    Product(item: "Chop Suey", quantity: 1, price: 50, isAvailable: true),
  ];
}

class Product {
  final int quantity;
  final double price;
  final String item;
  final bool isAvailable;
  Product({
    this.quantity,
    this.price,
    this.item,
    this.isAvailable,
  });
}
