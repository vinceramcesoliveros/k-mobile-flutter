import 'package:equatable/equatable.dart';

// To parse this JSON data, do
//
//     final kryptonTable = kryptonTableFromJson(jsonString);

import 'dart:convert';

class KryptonTable extends Equatable {
  final int id;
  final int tableGroupId;
  final String name;
  final int tableTypeId;
  final int positionX;
  final int positionY;
  final int isAvailable;
  final int isLocked;
  final int zIndex;
  final int rotation;
  final int merchantId;
  final String status;
  final String employeeName;
  final String orderCreated;
  final String createdOn;
  final String modifiedOn;

  KryptonTable({
    this.id,
    this.tableGroupId,
    this.name,
    this.tableTypeId,
    this.positionX,
    this.positionY,
    this.isAvailable,
    this.isLocked,
    this.zIndex,
    this.rotation,
    this.merchantId,
    this.status,
    this.employeeName,
    this.orderCreated,
    this.createdOn,
    this.modifiedOn,
  });

  KryptonTable copyWith({
    int id,
    int tableGroupId,
    String name,
    int tableTypeId,
    int positionX,
    int positionY,
    int isAvailable,
    int isLocked,
    int zIndex,
    int rotation,
    int merchantId,
    String status,
    String employeeName,
    String orderCreated,
    String createdOn,
    String modifiedOn,
  }) =>
      KryptonTable(
        id: id ?? this.id,
        tableGroupId: tableGroupId ?? this.tableGroupId,
        name: name ?? this.name,
        tableTypeId: tableTypeId ?? this.tableTypeId,
        positionX: positionX ?? this.positionX,
        positionY: positionY ?? this.positionY,
        isAvailable: isAvailable ?? this.isAvailable,
        isLocked: isLocked ?? this.isLocked,
        zIndex: zIndex ?? this.zIndex,
        rotation: rotation ?? this.rotation,
        merchantId: merchantId ?? this.merchantId,
        status: status ?? this.status,
        employeeName: employeeName ?? this.employeeName,
        orderCreated: orderCreated ?? this.orderCreated,
        createdOn: createdOn ?? this.createdOn,
        modifiedOn: modifiedOn ?? this.modifiedOn,
      );

  factory KryptonTable.fromRawJson(String str) =>
      KryptonTable.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory KryptonTable.fromJson(Map<String, dynamic> json) => KryptonTable(
        id: json["id"] == null ? null : json["id"],
        tableGroupId:
            json["tabpe_group_id"] == null ? null : json["tabpe_group_id"],
        name: json["name"] == null ? null : json["name"],
        tableTypeId:
            json["table_type_id"] == null ? null : json["table_type_id"],
        positionX: json["position_x"] == null ? null : json["position_x"],
        positionY: json["position_y"] == null ? null : json["position_y"],
        isAvailable: json["is_available"] == null ? null : json["is_available"],
        isLocked: json["is_locked"] == null ? null : json["is_locked"],
        zIndex: json["z_index"] == null ? null : json["z_index"],
        rotation: json["rotation"] == null ? null : json["rotation"],
        merchantId: json["merchant_id"] == null ? null : json["merchant_id"],
        status: json["status"] == null ? null : json["status"],
        employeeName:
            json["employee_name"] == null ? null : json["employee_name"],
        orderCreated:
            json["order_created"] == null ? null : json["order_created"],
        createdOn: json["created_on"] == null ? null : json["created_on"],
        modifiedOn: json["modified_on"] == null ? null : json["modified_on"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "tabpe_group_id": tableGroupId == null ? null : tableGroupId,
        "name": name == null ? null : name,
        "table_type_id": tableTypeId == null ? null : tableTypeId,
        "position_x": positionX == null ? null : positionX,
        "position_y": positionY == null ? null : positionY,
        "is_available": isAvailable == null ? null : isAvailable,
        "is_locked": isLocked == null ? null : isLocked,
        "z_index": zIndex == null ? null : zIndex,
        "rotation": rotation == null ? null : rotation,
        "merchant_id": merchantId == null ? null : merchantId,
        "status": status == null ? null : status,
        "employee_name": employeeName == null ? null : employeeName,
        "order_created": orderCreated == null ? null : orderCreated,
        "created_on": createdOn == null ? null : createdOn,
        "modified_on": modifiedOn == null ? null : modifiedOn,
      };

  @override
  List<Object> get props => [
        id,
        tableGroupId,
        name,
        tableTypeId,
        positionX,
        positionY,
        isAvailable,
        isLocked,
        zIndex,
        rotation,
        merchantId,
        status,
        employeeName,
        orderCreated,
        createdOn,
        modifiedOn,
      ];
}
