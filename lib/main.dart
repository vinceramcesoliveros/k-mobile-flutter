import 'package:flutter/material.dart';
import 'package:kmobile_flutter/widgets/kmobile_widgets.dart';
import 'pages/pages.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(KMobileApp());
}

class KMobileApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textTheme: TextTheme(
          body1: TextStyle(fontSize: 16.0),
        ),
      ),
      home: SafeArea(child: KStartScreen()),
      onGenerateRoute: onGenerateRoutes,
    );
  }
}

///
/*
 *
 * TODO: refactor onGenerateRoutes to different Folder. By any means of passing data to its route. Kindly refer
 to this tutorial
 [here](https://flutter.dev/docs/cookbook/navigation/navigate-with-arguments]
 */
Route onGenerateRoutes(RouteSettings settings) {
  switch (settings.name) {
    case "/settings":
      return PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => KSettings(),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          final begin = Offset(1.0, 0);
          final end = Offset.zero;
          final curve = Curves.ease;
          final tween = Tween(begin: begin, end: end);
          final curvedAnimation = CurvedAnimation(
            parent: animation,
            curve: curve,
          );
          return KSlideTransition(
            tween: tween,
            child: child,
            curvedAnimation: curvedAnimation,
          );
        },
      );
      break;
    case "/splashscreen":
      return PageRouteBuilder(pageBuilder: (context, _, __) => KSplashScreen());
      break;
    case "/dashboard":
      return PageRouteBuilder(pageBuilder: (context, _, __) => KDashboard());
    case "/access_code":
      return PageRouteBuilder(
        pageBuilder: (context, _, __) => KAccessCodeScreen(
          routeName: settings.arguments,
        ),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          final begin = Offset(0, 1);
          final end = Offset.zero;
          final curve = Curves.ease;
          return _kSlideTransition(begin, end, curve, animation, child);
        },
      );
    case "/table_ordering":
      return PageRouteBuilder(
        pageBuilder: (context, _, __) => KTableOrderScreen(),
      );
      break;
    case "/menu_orders":
      return PageRouteBuilder(
          pageBuilder: (context, _, __) => KOrderMenuScreen());
      break;
    default:
      return PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => KStartScreen(),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          final begin = Offset(1.0, 0);
          final end = Offset.zero;
          final curve = Curves.ease;
          return _kSlideTransition(begin, end, curve, animation, child);
        },
      );
  }
}

KSlideTransition _kSlideTransition(
  Offset begin,
  Offset end,
  Cubic curve,
  Animation animation,
  Widget child,
) {
  final tween = Tween(begin: begin, end: end);
  final curvedAnimation = CurvedAnimation(
    parent: animation,
    curve: curve,
  );
  return KSlideTransition(
    tween: tween,
    child: child,
    curvedAnimation: curvedAnimation,
  );
}
