import 'package:flutter/material.dart';

class KButton extends StatelessWidget {
  final Color color;
  final VoidCallback onPressed;
  final Widget child;

  KButton({
    this.color,
    @required this.child,
    @required this.onPressed,
  });
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: color ?? Colors.blueGrey[900],
      child: child,
      onPressed: onPressed,
    );
  }
}
