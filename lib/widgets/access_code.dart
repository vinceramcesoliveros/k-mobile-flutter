import 'package:flutter/material.dart';

typedef ResultAccessCode(bool accessCode);

class AccessCode extends StatelessWidget {
  final ResultAccessCode callback;

  AccessCode(this.callback);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: AccessCodeState(callback),
    );
  }
}

class AccessCodeState extends StatefulWidget {
  final ResultAccessCode callback;
  AccessCodeState(this.callback);
  AccessCodePage createState() => AccessCodePage();
}

class AccessCodePage extends State<AccessCodeState> {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width * .001;
    double screenHeight = MediaQuery.of(context).size.height * .001;
    List<String> input = [];
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.symmetric(horizontal: screenWidth * 330),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(
                "Enter the Access Code:",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: screenWidth * 25,
                    fontStyle: FontStyle.italic,
                    fontWeight: FontWeight.w300),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                height: screenHeight * 100,
                decoration: BoxDecoration(
                    color: Colors.black54,
                    borderRadius: BorderRadius.circular(100)),
                child: Container(
                  alignment: FractionalOffset.center,
                  child: ScrollConfiguration(
                    behavior: MyBehavior(),
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: input.length,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                              padding: EdgeInsets.all(screenWidth * 5),
                              child: new Icon(
                                Icons.lens,
                                size: screenWidth * 25,
                                color: Colors.white,
                              ));
                        }),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("1");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "1",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("2");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "2",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("3");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "3",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("4");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "4",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("5");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "5",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("6");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "6",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("7");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "7",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("8");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "8",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("9");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "9",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  InkWell(
                    onTap: () {},
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      child: Center(
                        child: Text(
                          "OK",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.add("0");
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(color: Colors.white)),
                      child: Center(
                        child: Text(
                          "0",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: screenWidth * 30),
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      setState(() {
                        input.clear();
                      });
                    },
                    borderRadius: BorderRadius.circular(50),
                    child: Container(
                      width: screenWidth * 80,
                      height: screenWidth * 80,
                      child: Center(
                        child: Icon(
                          Icons.backspace,
                          color: Colors.white,
                          size: screenWidth * 40,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
        Container(
            alignment: FractionalOffset.topRight,
            margin: EdgeInsets.only(right: screenWidth * 170),
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              borderRadius: BorderRadius.circular(50),
              child: Container(
                width: screenWidth * 80,
                height: screenWidth * 80,
                child: Center(
                  child: Icon(
                    Icons.close,
                    color: Colors.white54,
                    size: screenWidth * 60,
                  ),
                ),
              ),
            ))
      ],
    );
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
