import 'package:flutter/material.dart';

/// Customized ToggleButtons.
/// KToggleButton mus be wrapped in a stateful widget
/// in order to use the `setState` method.
///
class KToggleButtons extends StatelessWidget {
  const KToggleButtons({
    Key key,
    @required this.isSelected,
    @required this.onTap,
    @required this.children,
    this.color = Colors.white,
    this.fillColor,
    this.selectedColor = Colors.black,
  }) : super(key: key);
  final Function(int index) onTap;
  final List<bool> isSelected;
  final List<Widget> children;
  final Color color, selectedColor, fillColor;
  @override
  Widget build(BuildContext context) {
    return ToggleButtons(
      onPressed: onTap,
      isSelected: isSelected,
      selectedColor: selectedColor,
      color: color,
      fillColor: fillColor,
      children: children,
    );
  }
}
