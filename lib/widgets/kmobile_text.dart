import 'package:flutter/material.dart';

class KTitleWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 12.0),
      child: RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: "Krypton",
              style: TextStyle(
                fontWeight: FontWeight.w900,
                color: Colors.black,
                fontSize: 24,
              ),
            ),
            TextSpan(
              text: "mobile",
              style: TextStyle(
                color: Colors.yellow[700],
                fontWeight: FontWeight.w900,
                fontSize: 24,
              ),
            )
          ],
        ),
      ),
    );
  }
}
