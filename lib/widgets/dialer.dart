import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

///This widget is a modified version of the package called
/// [Flutter Dialer](https://pub.dev/packages/flutter_dialpad)
///
class DialPad extends StatefulWidget {
  final Color buttonColor;
  final Color buttonTextColor;
  final Color dialButtonColor;
  final Color dialButtonIconColor;
  final Function makeCall;
  final Orientation orientation;

  DialPad({
    this.makeCall,
    this.buttonColor,
    this.buttonTextColor,
    this.dialButtonColor,
    this.dialButtonIconColor,
    this.orientation,
  });

  @override
  _DialPadState createState() => _DialPadState();
}

class _DialPadState extends State<DialPad> {
  TextEditingController textEditingController;

  ///TODO: Remove constant access code if its in the database
  static const int access_code = 2121;
  var _value = "";
  static const mainTitle = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0",
  ];

  @override
  void initState() {
    super.initState();
    textEditingController = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();

    textEditingController.dispose();
  }

  _setText(String value) async {
    setState(() {
      _value += value;
      textEditingController.text = _value;
    });
  }

  List<Widget> _getDialerButtons() {
    var rows = List<Widget>();
    var items = List<Widget>();

    for (var i = 0; i < mainTitle.length; i++) {
      if (i % 3 == 0 && i > 0) {
        rows.add(Row(
            mainAxisAlignment: widget.orientation == Orientation.portrait
                ? MainAxisAlignment.spaceEvenly
                : MainAxisAlignment.center,
            children: items));
        rows.add(SizedBox(
          height: 12,
        ));
        items = List<Widget>();
      }

      items.add(Padding(
        padding: EdgeInsets.symmetric(horizontal: 12),
        child: DialButton(
          title: mainTitle[i],
          color: widget.buttonColor,
          textColor: widget.buttonTextColor,
          onTap: _setText,
        ),
      ));
    }
    //To Do: Fix this workaround for last row
    rows.add(Row(
        mainAxisAlignment: widget.orientation == Orientation.landscape
            ? MainAxisAlignment.spaceEvenly
            : MainAxisAlignment.center,
        children: items));
    rows.add(SizedBox(
      height: 12,
    ));

    return rows;
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var sizeFactor = screenSize.height * 0.09852217;

    return Column(
      children: <Widget>[
        Expanded(
            child: SizedBox(
          height: 8,
        )),
        Expanded(
          child: SizedBox(
            child: Text(
              "Enter Access Code",
              style: TextStyle(color: Colors.white, fontSize: 24),
            ),
          ),
        ),
        Expanded(
          child: SizedBox(
            child: Padding(
              padding: EdgeInsets.all(20),
              child: TextFormField(
                obscureText: true,
                readOnly: true,
                style: TextStyle(color: Colors.white, fontSize: sizeFactor / 2),
                textAlign: TextAlign.center,
                decoration: InputDecoration(border: InputBorder.none),
                controller: textEditingController,
              ),
            ),
          ),
        ),
        ..._getDialerButtons(),
        SizedBox(
          height: 15,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: Container(),
            ),
            Expanded(
              child: Center(
                child: DialButton(
                  icon: Icons.send,
                  color: Colors.green,
                  onTap: (value) {
                    if (_value == access_code.toString()) {
                      widget.makeCall();
                    } else {
                      Fluttertoast.showToast(msg: "Access Denied");
                    }
                    _value = "";
                    textEditingController.clear();
                  },
                ),
              ),
            ),
            Expanded(
              child: IconButton(
                icon: Icon(
                  Icons.backspace,
                  size: sizeFactor / 2,
                  color: Colors.red,
                ),
                onPressed: _value.length == 0
                    ? null
                    : () {
                        if (_value != null && _value.length > 0) {
                          setState(() {
                            _value = "";
                            textEditingController.clear();
                          });
                        }
                      },
              ),
            ),
          ],
        ),
        Expanded(child: SizedBox()),
        Expanded(
          child: SizedBox(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: MediaQuery.of(context).size.width / 2,
                  child: FlatButton(
                    color: Colors.red,
                    child: Text("BACK",
                        style: TextStyle(color: Colors.white, fontSize: 24)),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }
}

class DialButton extends StatefulWidget {
  final Key key;
  final String title;
  final String subtitle;
  final Color color;
  final Color textColor;
  final IconData icon;
  final Color iconColor;
  final ValueSetter<String> onTap;
  final bool shouldAnimate;
  DialButton(
      {this.key,
      this.title,
      this.subtitle,
      this.color,
      this.textColor,
      this.icon,
      this.iconColor,
      this.shouldAnimate,
      this.onTap});

  @override
  _DialButtonState createState() => _DialButtonState();
}

class _DialButtonState extends State<DialButton>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation _colorTween;
  Timer _timer;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
    _colorTween = ColorTween(
            begin: widget.color != null ? widget.color : Colors.white24,
            end: Colors.white)
        .animate(_animationController);
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer?.cancel();
    }
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    var sizeFactor = screenSize.height * 0.09852217;

    return GestureDetector(
      onTap: () {
        if (this.widget.onTap != null) this.widget.onTap(widget.title);

        if (widget.shouldAnimate == null || widget.shouldAnimate) {
          if (_animationController.status == AnimationStatus.completed) {
            _animationController.reverse();
          } else {
            _animationController.forward();
            _timer = Timer(const Duration(milliseconds: 200), () {
              setState(() {
                _animationController.reverse();
              });
            });
          }
        }
      },
      child: ClipOval(
          child: AnimatedBuilder(
              animation: _colorTween,
              builder: (context, child) => Container(
                    color: _colorTween.value,
                    height: sizeFactor,
                    width: sizeFactor,
                    child: Center(
                        child: widget.icon == null
                            ? widget.subtitle != null
                                ? Column(
                                    children: <Widget>[
                                      Padding(
                                          padding: EdgeInsets.only(top: 8),
                                          child: Text(
                                            widget.title,
                                            style: TextStyle(
                                                fontSize: sizeFactor / 2,
                                                color: widget.textColor != null
                                                    ? widget.textColor
                                                    : Colors.white),
                                          )),
                                    ],
                                  )
                                : Text(
                                    widget.title,
                                    style: TextStyle(
                                        fontSize: widget.title == "*" &&
                                                widget.subtitle == null
                                            ? screenSize.height * 0.0862069
                                            : sizeFactor / 2,
                                        color: widget.textColor != null
                                            ? widget.textColor
                                            : Colors.white),
                                  )
                            : Icon(widget.icon,
                                size: sizeFactor / 2,
                                color: widget.iconColor != null
                                    ? widget.iconColor
                                    : Colors.white)),
                  ))),
    );
  }
}
