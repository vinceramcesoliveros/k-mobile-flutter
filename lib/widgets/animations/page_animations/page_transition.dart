import 'package:flutter/material.dart';

class KSlideTransition extends StatefulWidget {
  final Tween tween;
  final CurvedAnimation curvedAnimation;
  final Widget child;
  KSlideTransition({
    @required this.tween,
    @required this.curvedAnimation,
    @required this.child,
  });
  @override
  _KSlideTransitionState createState() => _KSlideTransitionState();
}

class _KSlideTransitionState extends State<KSlideTransition> {
  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: widget.tween.animate(widget.curvedAnimation),
      child: widget.child,
    );
  }
}
