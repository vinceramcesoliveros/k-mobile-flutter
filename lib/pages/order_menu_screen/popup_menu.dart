import 'package:flutter/material.dart';

class KPopupReferenceButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(onSelected: (String path) {
      /// TODO: Logic for Print & Edit References.
      /// For the `Edit` Button, Just use Default Dialog
      /// that can have input and must return a string
      /// for the `Print` Button, it needs to detect if there
      /// are Printers available.
    }, itemBuilder: (context) {
      return [
        PopupMenuItem(
          value: "Edit",
          child: Text("Edit References"),
        ),
        PopupMenuItem(
          value: "Print",
          child: Text("Print References"),
        ),
      ];
    });
  }
}
