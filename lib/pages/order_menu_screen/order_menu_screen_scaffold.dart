import 'package:badges/badges.dart';
import 'package:flutter/material.dart';

import '../../fixtures/krypton_weekly_data.dart';
import '../../widgets/kmobile_raised_button.dart';
import '../../widgets/kmobile_toggle_buttons.dart';
import '../../widgets/vertical_tab_widget/vertical_tabs.dart';
import 'popup_menu.dart';

class KOrderMenuScreen extends StatefulWidget {
  const KOrderMenuScreen({
    Key key,
  }) : super(key: key);
  @override
  _KOrderMenuScreenState createState() => _KOrderMenuScreenState();
}

class _KOrderMenuScreenState extends State<KOrderMenuScreen> {
  List<bool> isSelected = [true, false];
  List<bool> weekSelected = [
    true,
    false,
    false,
    false,
    false,
  ];
  int currentPage = 0;
  int selectedIndex = 0;
  PageController controller;
  @override
  void initState() {
    super.initState();
    controller = PageController(initialPage: currentPage);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: Container(
          width: 370,
          child: KDrawerOrderMenu(),
        ),
        drawerEdgeDragWidth: 0,
        appBar: AppBar(
          leading: Builder(
            builder: (context) => IconButton(
              ///TODO: Use the length of orders from bloc to update the number of orders for the badge.
              ///
              icon: Badge(
                badgeContent: Text("0"),
                child: Icon(Icons.call_to_action),
                badgeColor: Colors.cyan,
              ),
              onPressed: () {
                Scaffold.of(context).openDrawer();
              },
            ),
          ),
          title: Text(""),
          backgroundColor: Colors.black,
          actions: <Widget>[
            KToggleButtons(
              color: Colors.white,
              selectedColor: Colors.yellow,
              fillColor: Colors.transparent,
              isSelected: isSelected,
              onTap: _onTogglePageView,
              children: <Widget>[
                Icon(
                  Icons.apps,
                ),
                Icon(
                  Icons.menu,
                )
              ],
            ),
            KPopupReferenceButton()
          ],
        ),
        body: KVerticalTabs(),
      ),
    );
  }

  void _onTogglePageView(int index) {
    setState(() {
      for (int indexBtn = 0; indexBtn < isSelected.length; indexBtn++) {
        if (indexBtn == index) {
          isSelected[indexBtn] = true;
          currentPage = indexBtn;
        } else {
          isSelected[indexBtn] = false;
        }
        if (currentPage == 1) {
          controller.animateTo(MediaQuery.of(context).size.width,
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        } else {
          controller.animateTo(0,
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        }
      }
    });
  }
}

class KDrawerOrderMenu extends StatelessWidget {
  const KDrawerOrderMenu({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              color: Colors.black87,
              padding: EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Quantity",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                  Text("Item",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 12,
                      )),
                  Text(
                    "Total",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 16,
            child: Container(),
          ),
          Expanded(
            flex: 3,
            child: Container(
              color: Colors.grey[300],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Text("Total:"),
                      Text("Tax:"),
                      Text("Check Discount:"),
                      Text(
                        "Grand Total:",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text("0.00"),
                      Text("0.00"),
                      Text("-0.00"),
                      Text(
                        "0.00",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
                color: Colors.black87,
                child: Center(
                  child: Text(
                    "Settle",
                    style: TextStyle(color: Colors.white),
                  ),
                )),
          ),
        ],
      ),
    );
  }
}

class KVerticalTabs extends StatefulWidget {
  KVerticalTabs({Key key}) : super(key: key);
  @override
  _KVerticalTabsState createState() => _KVerticalTabsState();
}

class _KVerticalTabsState extends State<KVerticalTabs> {
  final List<String> fakeData = FakeWeeklyData.fakeWeeklyData;
  final List<Product> products = FakeWeeklyData.products;
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return VerticalTabs(
      tabsWidth: 100,
      selectedTabTextStyle: TextStyle(fontSize: 14),
      selectedTabBackgroundColor: Colors.cyan,
      tabs: fakeData.map((data) {
        return Tab(text: data);
      }).toList(),
      contents: fakeData.map((data) {
        return WeekLunchMenu(
          fakeData: products,
        );
      }).toList(),
    );
  }
}

class WeekLunchMenu extends StatefulWidget {
  const WeekLunchMenu({
    @required this.fakeData,
    Key key,
  }) : super(key: key);

  final List<Product> fakeData;

  @override
  _WeekLunchMenuState createState() => _WeekLunchMenuState();
}

class _WeekLunchMenuState extends State<WeekLunchMenu> {
  int selectedIndex = 0;
  final List<String> fakeDays = FakeWeeklyData.fakeDays;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Scrollbar(
            child: ListView.builder(
                itemCount: fakeDays.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 8),
                    padding: const EdgeInsets.symmetric(horizontal: 8),
                    child: MaterialButton(
                        minWidth: 100,
                        height: 5,
                        onPressed: () {
                          setState(() {
                            /// TODO: Update fakeData based on selected Index,
                            /// Onto next feature, fetch data for each click
                            /// based on the text of the widget.
                            if (selectedIndex != index) {
                              selectedIndex = index;
                            }
                          });
                        },
                        child: Text(fakeDays[index]),
                        color: selectedIndex == index
                            ? Colors.orange[700]
                            : Colors.orange[200]),
                  );
                }),
          ),
        ),
        Divider(color: Colors.black),
        Expanded(
          flex: 8,
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: GridView.count(
                    childAspectRatio: 1,
                    crossAxisCount: 3,
                    children: widget.fakeData.map((data) {
                      return GestureDetector(
                        onTap: () {
                          ///TODO: Add Items to the Drawer for Orders.
                        },
                        child: Card(
                            child: Column(
                          children: <Widget>[
                            Text(data.item),
                            Text("${data.price}"),
                          ],
                        )),
                      );
                    }).toList(),
                  ),
                )
              ],
            ),
          ),
        ),
        TableOrderActionButtons()
      ],
    );
  }
}

class TableOrderActionButtons extends StatelessWidget {
  const TableOrderActionButtons({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: KButton(
              child: Text("SEND ORDER", style: TextStyle(color: Colors.white)),
              onPressed: () {},
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: KButton(
              child: Text("CANCEL", style: TextStyle(color: Colors.white)),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ),
        ],
      ),
    );
  }
}
