import 'dart:async';

import 'package:flutter/material.dart';
import 'package:kmobile_flutter/widgets/kmobile_image.dart';
import 'package:kmobile_flutter/widgets/kmobile_widgets.dart';

class KSplashScreen extends StatefulWidget {
  @override
  _KSplashScreenState createState() => _KSplashScreenState();
}

class _KSplashScreenState extends State<KSplashScreen> {
  Timer timer;
  @override
  void initState() {
    super.initState();
    int _percent = 0;

    timer = Timer.periodic(Duration(seconds: 1), (time) {
      _percent += 50;
      if (_percent == 100) {
        Navigator.pushReplacementNamed(context, "/dashboard");
      }
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              KImageLogo(),
              KTitleWidget(),
              CircularProgressIndicator(
                backgroundColor: Colors.yellow,
                valueColor: AlwaysStoppedAnimation(Colors.white),
              )

              /// Text Update Here, use Bloc to return the text messages.
            ],
          ),
        ),
      ),
    );
  }
}
