import 'package:flutter/material.dart';
import 'package:kmobile_flutter/models/krypton_table/krypton_table.dart';

/// TODO: Use this widget both Dining and Party Tab.
class PositionedTableOrder extends StatelessWidget {
  const PositionedTableOrder({
    @required this.kryptonTables,
  });

  /// TODO: Not final impelemtation for KryptonTable
  final List<KryptonTable> kryptonTables;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: SizedBox(
            child: Stack(
              children: kryptonTables.map((table) {
                return Positioned(
                  top: table.positionY?.toDouble(),
                  left: table.positionX?.toDouble(),
                  child: TableWidget(
                    table: table,
                  ),
                );
              }).toList(),
            ),
          ),
        )
      ],
    );
  }
}

class TableWidget extends StatelessWidget {
  const TableWidget({
    @required this.table,
  });

  final KryptonTable table;

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed("/menu_orders");
      },
      child: Column(
        children: <Widget>[
          Text(table.name ?? ""),
          Container(
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Image.asset("assets/images/square_table.png",
                    fit: BoxFit.fill, width: width / 10),
                Text(table.name),
              ],
            ),
          ),
          Text(table.createdOn ?? ""),
        ],
      ),
    );
  }
}
