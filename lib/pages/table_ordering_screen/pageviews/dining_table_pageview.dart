import 'package:flutter/material.dart';
import 'package:kmobile_flutter/fixtures/krypton_table_data.dart';
import 'package:kmobile_flutter/pages/table_ordering_screen/gridviews/table_order_gridview.dart';

import '../../../widgets/zoomable_widget/zoomable_widget.dart';
import '../positioned_table_orders.dart';

class DiningTableOrdersPage extends StatelessWidget {
  const DiningTableOrdersPage({
    @required this.pageController,
  });

  final PageController pageController;

  @override
  Widget build(BuildContext context) {
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: pageController,
      children: [
        Zoom(
            colorScrollBars: Colors.black,
            opacityScrollBars: 1,
            scrollWeight: 10,
            centerOnScale: false,
            onPositionUpdate: (offset) => Offset(0, 0),
            backgroundColor: Colors.white,
            width: 900,
            height: 600,
            child: PositionedTableOrder(
              kryptonTables: FakeKryptonTable.fakeTable,
            )),

        ///GridView List
        GridViewTableOrder(
          kryptonTables: FakeKryptonTable.fakeTable,
        ),
      ],
    );
  }
}
