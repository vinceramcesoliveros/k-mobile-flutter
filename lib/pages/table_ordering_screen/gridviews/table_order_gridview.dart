import 'package:flutter/material.dart';
import 'package:kmobile_flutter/models/krypton_table/krypton_table.dart';

class GridViewTableOrder extends StatefulWidget {
  final List<KryptonTable> kryptonTables;

  const GridViewTableOrder({
    this.kryptonTables,
    Key key,
  }) : super(key: key);

  @override
  _GridViewTableOrderState createState() => _GridViewTableOrderState();
}

class _GridViewTableOrderState extends State<GridViewTableOrder> {
  @override
  Widget build(BuildContext context) {
    return OrientationBuilder(builder: (context, orientation) {
      return GridView.count(
        childAspectRatio: (1.05),
        crossAxisCount: orientation == Orientation.portrait ? 3 : 4,
        children: widget.kryptonTables.map((table) {
          /// TODO: Change the color according to `isAvailable` property
          return GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed("/menu_orders");
            },
            child: Card(
              child: SizedBox(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Customer Name",
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "${table.name}",
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "${table.createdOn ?? '0m'}",
                          textAlign: TextAlign.end,
                        )
                      ],
                    ),
                    ConstrainedBox(
                      constraints: BoxConstraints(minWidth: double.infinity),
                      child: Container(
                        margin: EdgeInsets.symmetric(
                            vertical: 0.5, horizontal: 0.1),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(6),
                              bottomRight: Radius.circular(6),
                            ),
                            border: Border.all(color: Colors.grey, width: 0.8),
                            color: Colors.white),
                        child: Text(
                          table.isAvailable == 0 ? "Used" : "Available",
                          textAlign: TextAlign.center,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        }).toList(),
      );
    });
  }
}
