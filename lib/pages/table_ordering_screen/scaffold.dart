import 'package:flutter/material.dart';
import 'package:kmobile_flutter/widgets/kmobile_toggle_buttons.dart';

import 'pageviews/dining_table_pageview.dart';
import 'pageviews/party_table_pageview.dart';

class KTableOrderScreen extends StatefulWidget {
  const KTableOrderScreen({
    Key key,
  }) : super(key: key);
  @override
  _KTableOrderScreenState createState() => _KTableOrderScreenState();
}

class _KTableOrderScreenState extends State<KTableOrderScreen> {
  final List<bool> isSelected = [true, false];
  int currentPage = 0;
  PageController pageController = PageController();

  @override
  void initState() {
    super.initState();
    pageController = PageController(initialPage: currentPage);
  }

  @override
  void dispose() {
    super.dispose();
    pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            KToggleButtons(
              isSelected: isSelected,
              onTap: _onTogglePageView,
              color: Colors.white,
              selectedColor: Colors.yellow,
              fillColor: Colors.transparent,
              children: <Widget>[
                Icon(
                  Icons.apps,
                ),
                Icon(
                  Icons.menu,
                )
              ],
            ),
          ],
          backgroundColor: Colors.black,
          bottom: TabBar(
            onTap: (index) {
              isSelected[0] = true;
              isSelected[1] = false;
              setState(() {
                pageController.animateTo(0,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeIn);
              });
            },
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.local_dining),
                text: "Dining",
              ),
              Tab(icon: Icon(Icons.local_activity), text: 'Party')
            ],
          ),
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            DiningTableOrdersPage(
              pageController: pageController,
            ),
            PartyTableOrdersPage(pageController: pageController)
          ],
        ),
      ),
    );
  }

  void _onTogglePageView(int index) {
    setState(() {
      for (int indexBtn = 0; indexBtn < isSelected.length; indexBtn++) {
        if (indexBtn == index) {
          isSelected[indexBtn] = true;
          currentPage = indexBtn;
        } else {
          isSelected[indexBtn] = false;
        }
        if (currentPage == 1) {
          pageController.animateTo(MediaQuery.of(context).size.width,
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        } else {
          pageController.animateTo(0,
              duration: Duration(milliseconds: 300), curve: Curves.easeIn);
        }
      }
    });
  }
}
