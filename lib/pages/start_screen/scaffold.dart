import 'package:flutter/material.dart';
import 'package:kmobile_flutter/widgets/kmobile_image.dart';
import 'package:kmobile_flutter/widgets/kmobile_widgets.dart';

import 'krypton_button.dart';

class KStartScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.settings),
            onPressed: () async {
              await Navigator.of(context).pushNamed('/settings');
            },
          ),
        ],
        title: Text("KMobile"),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              KImageLogo(),
              KTitleWidget(),
              KStartButtonWidget()
            ],
          ),
        ),
      ),
    );
  }
}
