import 'package:flutter/material.dart';

class KStartButtonWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 1.75,
      height: 42,
      child: RaisedButton(
        color: Colors.black,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(40.0),
        ),
        child: Text(
          "START",
          style: TextStyle(
            fontWeight: FontWeight.w900,
            color: Colors.white,
          ),
        ),
        onPressed: () {
          // TODO: Check if settings have initialize
          Navigator.of(context).pushReplacementNamed(
            "/splashscreen",
          );
        },
      ),
    );
  }
}
