import 'package:flutter/material.dart';

class KSettingSideBar extends StatefulWidget {
  @override
  _KSettingSideBarState createState() => _KSettingSideBarState();
}

class _KSettingSideBarState extends State<KSettingSideBar> {
  List<bool> isSelected = [true, false];
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: Colors.grey[300],
        child: Column(
          children: <Widget>[
            RotatedBox(
              quarterTurns: 1,
              child: ToggleButtons(
                isSelected: isSelected,
                renderBorder: false,
                splashColor: Colors.transparent,
                selectedColor: Colors.white,
                fillColor: Colors.yellow[700],
                onPressed: (index) {
                  setState(() {
                    for (int i = 0; i < isSelected.length; i++) {
                      if (i == index) {
                        isSelected[i] = true;
                      } else {
                        isSelected[i] = false;
                      }
                    }
                  });
                },
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: RotatedBox(
                      quarterTurns: 3,
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.public, size: 36),
                          Text("Web Service")
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: RotatedBox(
                      quarterTurns: 3,
                      child: Column(
                        children: <Widget>[
                          Icon(Icons.storage, size: 36),
                          Text("Database")
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
