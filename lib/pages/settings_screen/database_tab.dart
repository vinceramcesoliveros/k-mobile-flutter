import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class KDatabaseTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          "Database Settings",
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.router),
                labelText: "Host",
                border: OutlineInputBorder()),
            onChanged: (value) {
              // Save value in the Globals() or Singleton
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.person),
                labelText: "Username",
                border: OutlineInputBorder()),
            onChanged: (value) {
              // Save value in the Globals() or Singleton
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            obscureText: true,
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.lock),
                suffixIcon: IconButton(
                  icon: Icon(Icons.remove_red_eye),
                  onPressed: () {},
                ),
                labelText: "Password",
                border: OutlineInputBorder()),
            onChanged: (value) {
              // Save value in the Globals() or Singleton
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.storage),
                labelText: "Database Name",
                border: OutlineInputBorder()),
            onChanged: (value) {
              // Save value in the Globals() or Singleton
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.dns),
                labelText: "Port",
                border: OutlineInputBorder()),
            onChanged: (value) {
              // Save value in the Globals() or Singleton
            },
          ),
        ),
        RaisedButton(
          onPressed: () {
            /// Checking Connection will refer to Status Code [200].
            Fluttertoast.showToast(msg: "Connected!");
          },
          child: Text("Check Connection"),
        )
      ],
    );
  }
}
