import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class KWebServiceTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.public),
              labelText: "Web Service",
              border: OutlineInputBorder(),
            ),
            keyboardAppearance: Brightness.dark,
          ),
        ),
        RaisedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: "Connected!");
          },
          child: Text("Check Connection"),
        ),
      ],
    );
  }
}
