import 'package:flutter/material.dart';

import 'database_tab.dart';
import 'printer_tab.dart';
import 'web_service.dart';

class KSettings extends StatefulWidget {
  KSettings({
    Key key,
  }) : super(key: key);
  @override
  _KSettingsState createState() => _KSettingsState();
}

class _KSettingsState extends State<KSettings> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
          resizeToAvoidBottomInset: true,
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Text("Settings"),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.check),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              IconButton(
                icon: Icon(Icons.close),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
            bottom: TabBar(
              tabs: <Widget>[
                Tab(icon: Icon(Icons.public), text: "Web Service"),
                Tab(icon: Icon(Icons.storage), text: "Database"),
                Tab(icon: Icon(Icons.print), text: "Printer"),
              ],
            ),
          ),

          /// TODO: Revamp the UI for settings. Looks disorganized.
          body: TabBarView(children: [
            /// Web Service Layout
            KWebServiceTab(),
            KDatabaseTab(),
            KPrinterTab(),

            /// Database Layout
          ])),
    );
  }
}
