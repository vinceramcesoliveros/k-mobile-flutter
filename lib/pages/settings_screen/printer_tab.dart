import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class KPrinterTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Text(
          "Printer Settings",
          style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.router),
                labelText: "Host",
                border: OutlineInputBorder()),
            onChanged: (value) {
              // Save value in the Globals() or Singleton
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.person),
                labelText: "Printer Port",
                border: OutlineInputBorder()),
            onChanged: (value) {
              // Save value in the Globals() or Singleton
            },
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextFormField(
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.lock),
              suffixIcon: IconButton(
                icon: Icon(Icons.remove_red_eye),
                onPressed: () {},
              ),
              labelText: "Printer Font Size",
              border: OutlineInputBorder(),
            ),
            onChanged: (value) {
              // Save value in the Globals() or Singleton
            },
          ),
        ),
        Padding(padding: const EdgeInsets.all(8.0), child: KDropDownPrinter()),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            OutlineButton(
              child: Text("Save and Test", style: TextStyle(fontSize: 16)),
              onPressed: () {},
            ),
            OutlineButton(
              child: Text("Refresh List", style: TextStyle(fontSize: 16)),
              onPressed: () {},
            ),
          ],
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width / 1.25,
          child: RaisedButton(
            onPressed: () {
              /// Checking Connection will refer to Status Code [200].
              Fluttertoast.showToast(msg: "Connected!");
            },
            child: Text("Check Connection"),
          ),
        )
      ],
    );
  }
}

class KDropDownPrinter extends StatefulWidget {
  @override
  _KDropDownPrinterState createState() => _KDropDownPrinterState();
}

class _KDropDownPrinterState extends State<KDropDownPrinter> {
  String dropDownValue = "Select Printer";
  @override
  Widget build(BuildContext context) {
    return DropdownButton<String>(
        value: dropDownValue,
        items: [
          DropdownMenuItem<String>(
            child: Text("Select Printer"),
            value: "Select Printer",
          )
        ],
        onChanged: (value) {
          setState(() {
            dropDownValue = value;
          });
        });
  }
}
