import 'dart:io';

import 'package:flutter/material.dart';

import 'route_buttons.dart';

class KDashboard extends StatelessWidget {
  const KDashboard({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Future<bool> _onPop() async {
      final bool result = await showDialog<bool>(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text("Do you want to exit App?"),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context, false);
                  },
                  child: Text("No"),
                ),
                RaisedButton(
                  color: Colors.yellow[700],
                  onPressed: () {
                    exit(0);
                  },
                  child: Text("Yes", style: TextStyle(color: Colors.black)),
                )
              ],
            );
          });
      return result;
    }

    return WillPopScope(
      onWillPop: _onPop,
      child: Scaffold(
        appBar: AppBar(
          /// Todo, Get the device name of the user,
          title: Text("Welcome back User"),
          backgroundColor: Colors.black87,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.settings),
              onPressed: () {
                Navigator.of(context).pushNamed("/settings");
              },
            )
          ],
        ),
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                KDashboardRouteButton(
                  icon: Icons.developer_board,
                  text: "Table Ordering",
                  onTap: () async {
                    Navigator.of(context).pushNamed("/access_code",
                        arguments: "/table_ordering");
                  },
                ),
                KDashboardRouteButton(
                  icon: Icons.account_balance_wallet,
                  text: "Power Bill",
                  onTap: () async {
                    Navigator.of(context)
                        .pushNamed("/access_code", arguments: "/power_bills");
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
