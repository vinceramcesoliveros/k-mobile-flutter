import 'package:flutter/material.dart';

/// Note: This widget is not reusable and is not recommended
/// to use it in other widgets.
/// You may use it only for the dashboard, but not on other pages.
class KDashboardRouteButton extends StatelessWidget {
  final IconData icon;
  final String text;
  final VoidCallback onTap;
  KDashboardRouteButton({
    this.text,
    this.icon,
    this.onTap,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SizedBox(
        width: MediaQuery.of(context).size.width / 2.75,
        child: Container(
          height: 150,
          margin: EdgeInsets.symmetric(vertical: 8),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4),
              border: Border.all(color: Colors.black, width: 1)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 16),
                child: Icon(icon, size: 64),
              ),
              Text(
                text,
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
