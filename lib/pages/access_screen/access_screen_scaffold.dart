import 'package:flutter/material.dart';
import 'package:kmobile_flutter/widgets/dialer.dart';

/// Use this widget if you there are any Access Code Validation
/// TODO: Evaluate if the Access code comes from the database
/// or a Constant value only.
class KAccessCodeScreen extends StatefulWidget {
  final String routeName;
  KAccessCodeScreen({this.routeName, Key key}) : super(key: key);
  @override
  _KAccessCodeScreenState createState() => _KAccessCodeScreenState();
}

class _KAccessCodeScreenState extends State<KAccessCodeScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: OrientationBuilder(
        builder: (context, orientation) => DialPad(
          orientation: orientation,
          makeCall: () {
            Navigator.pushReplacementNamed(
              context,
              widget.routeName,
            );
          },
        ),
      ),
    );
  }
}
