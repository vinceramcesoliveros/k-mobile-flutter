# K-Mobile Flutter

Application for dealing with Table Orders and capable of Printing receipts.
The Application was rewritten from Android to Flutter.


> The Source code of the previous project is outdated and cannot be fixed.

## Requirements
You must have the following SDK installed in your workstation.
- [Android Studio](https://developer.android.com/studio)
- [Emulator - Geny Motion](https://www.genymotion.com/) or Any other Android Emulators you can find.
### Windows:
1. **Install Flutter SDK**
   Download the **Flutter SDK** [here](https://flutter.dev). Once you have downloaded the SDK. be sure to read the instructions **carefully**.
2. Locate your Dart SDK inside `$YOUR_FLUTTER_DIRECTORY\flutter\bin\cache\dart-sdk\bin`. Copy it and put it inside your Enviroment PATH(Note: The tutorial of adding flutter to your environment path is the same as Dart SDK).
   ![dart_flutter_env](./k-mobile&#32;doc&#32;screenshot/dart_and_flutter_env.png)
```sh
# TODO: Create a simple screenshot available here in README.MD

# Activate Flutter CLI
flutter doctor -v

# Activate Dartdoc
pub global activate dartdoc
```

## Project Structure

Each Screen will have its own Folder inside the Pages, Widgets that are reusable can be separated into another folder.


## Code Documentation
> TODO: Generate Tutorial on dartdoc to generate Dart Documentation.

### Steps:
To read the dartdoc tutorial, click [here](https://pub.dev/packages/dartdoc) to know more.
